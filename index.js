#!/usr/bin/env node
const fetch = require('node-fetch')

let usage = `
Usage:
npm-dependency-tree <npm package name>

Options:
-v = verbose, displays dependecies in an indented tree like structure
-j = json, displays total of nested dependencies for each package

Examples:
npm-dependency-tree pg
npm-dependency-tree pg -v
npm-dependency-tree pg -j
`.trim()

let full_arg_list = process.argv.slice(2)

// arguments
let arg_list = full_arg_list.filter(a=>!a.startsWith('-'))
let arg_list_length = arg_list.length

// show usage and exit
if (arg_list_length < 1) {
  console.log(usage)
  process.exit()
}

// flag variables
let verbose = false
let output_json = false

// flag arguments
let flag_list = full_arg_list.filter(a=>a.startsWith('-'))

// flag handlers
if (flag_list.includes('-v')) {
  verbose = true
}
if (flag_list.includes('-j')) {
  output_json = true
}


let npm_package_name = arg_list[0]
let registry_base_url = 'https://registry.npmjs.org/'
let visited_package_dependency_count_map = {}
let dependency_tree_list = []

async function get_dependency_count(package_name, depth_level=0) {
  // Mark package as visited with a nonsense number of dependencies
  visited_package_dependency_count_map[package_name] = -1


  let dependency_count = 0
  let registry_json = await fetch(`https://registry.npmjs.org/${package_name}`).then(async r => await r.json())
  let latest_version = registry_json['dist-tags'].latest
  let version_json = registry_json.versions[latest_version]

  // If the json does NOT have the property 'dependencies', default to empty array
  let current_package_dependency_list = version_json.hasOwnProperty('dependencies') ? Object.keys(version_json.dependencies) : []
  let current_package_dependency_count = current_package_dependency_list.length

  // Save this package and its dependency count to the set of visited packages
  visited_package_dependency_count_map[package_name] = dependency_count

  if (version_json.hasOwnProperty('dependencies')) {
    // for each dependency
    for (let dep in version_json.dependencies) {
      dependency_count += 1

      // If this package does not have dependency count recorded yet
      if (visited_package_dependency_count_map[package_name] < 0) {
        visited_package_dependency_count_map[package_name] = current_package_dependency_count
      }

      // If this dependency of the current package was already visited and has a dependency count saved
      if (visited_package_dependency_count_map.hasOwnProperty(dep) && visited_package_dependency_count_map[dep] > -1) {
        // Add the saved count to the current dependency count and do not re-fetch it to avoid circular logic
        dependency_count += visited_package_dependency_count_map[dep]
      } else {
        // Else, we have not visited this dependency yet
        dependency_count += await get_dependency_count(dep, depth_level + 1)
      }
    }

    dependency_tree_list.push('  '.repeat(depth_level) + `[${package_name}] has [${current_package_dependency_count}] dependencies. [${current_package_dependency_list}]`)

    // console.log(`[${package_name}] has [${Object.keys(version_json.dependencies).length}] dependencies. [${Object.keys(version_json.dependencies)}]`)
  }

  // Save this package and its dependency count to the set of visited packages
  visited_package_dependency_count_map[package_name] = dependency_count


  return dependency_count
}

async function run_command(package_name) {
  let count = await get_dependency_count(package_name).catch(console.log)

  if (!output_json) {  
    // Dependency counts are top level only
    console.log(`Total dependency count for [${package_name}]: ${count}`)
  } else {
    // Dependency counts are totals including nested dependencies
    console.log(JSON.stringify(visited_package_dependency_count_map))
  }

  if (verbose) {
    // show the dependency tree
    console.log('')
    dependency_tree_list.reverse().forEach((s) => {
      console.log(s)
    })
  }

  process.exit()
}

run_command(npm_package_name)
