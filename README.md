# README #

### What is this repository for? ###

This project aims to show the true number of dependencies an npm package has by crawling the entire dependency tree.

### How do I get set up? ###

1.  Run `npm i -g npm-dependency-tree` to globally install
2.  To use run `npm-dependency-tree <package name>`


Examples:

Verbose (-v)
```
$ npm-dependency-tree npm-dependency-tree -v
Total dependency count for [npm-dependency-tree]: 1

[npm-dependency-tree] has [1] dependencies. [node-fetch]
  [node-fetch] has [0] dependencies. []
```

JSON (-j)
```
$ npm-dependency-tree -j
{"npm-dependency-tree":1,"node-fetch":0}
```
